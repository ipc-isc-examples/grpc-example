import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.google.protobuf.gradle.id

plugins {
    kotlin("jvm") version "1.7.21"
    id("com.google.protobuf") version "0.9.1"
    application
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(11))
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().all {
    kotlinOptions {
        freeCompilerArgs = listOf("-opt-in=kotlin.RequiresOptIn")
    }
}

group = "org.school.isc"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    gradlePluginPortal()
}

dependencies {
    api(kotlin("stdlib-jdk8"))
    api("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")


    api("io.grpc:grpc-stub:1.51.0")
    api("io.grpc:grpc-protobuf:1.51.0")
    api("com.google.protobuf:protobuf-java-util:3.21.9")
    api("com.google.protobuf:protobuf-kotlin:3.21.9")
    api("io.grpc:grpc-kotlin-stub:1.3.0")


    testImplementation(kotlin("test"))
    runtimeOnly("com.google.protobuf:protobuf-gradle-plugin:0.9.1")
    implementation("io.grpc:grpc-all:1.51.0")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

application {
    mainClass.set("MainKt")
}

protobuf {
    protoc {
        artifact = "com.google.protobuf:protoc:3.21.9"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:1.51.0"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:1.3.0:jdk8@jar"
        }
    }
    generateProtoTasks {
        all().forEach {
            it.plugins {
                id("grpc")
                id("grpckt")
            }
            it.builtins {
                id("kotlin")
            }
        }
    }
}