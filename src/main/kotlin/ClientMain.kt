import io.grpc.ManagedChannelBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(args: Array<String>) {
    val channel = ManagedChannelBuilder.forAddress("localhost", 8081)
        .usePlaintext()
        .build()

    runBlocking {
        launch(Dispatchers.Default) {
            val stub = PersonServiceGrpcKt.PersonServiceCoroutineStub(channel)
            val persons = stub.getPerson(
                flow {
                    while (true) {
                        val idFromCli = readln()
                        emit(personId { id = idFromCli })
                    }
                }
            )
            launch {
                persons.collect {
                    println(modifyMultilineString(it.toString(), 1))
                }
            }
        }
    }
}

private fun modifyMultilineString(value: String, indent: Int): String {
    return value
        .split("\n")
        .joinToString("\n") {
            takeTabTimes(indent) + it
        }
}

private fun takeTabTimes(times: Int): String {
    return takeTimes("\t", times)
}

private fun takeTimes(elem: String, times: Int): String {
    return List(times) { _ -> elem }.joinToString("")
}