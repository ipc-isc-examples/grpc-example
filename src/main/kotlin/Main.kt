import io.grpc.ServerBuilder
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class PersonService : PersonServiceGrpcKt.PersonServiceCoroutineImplBase() {
    override fun getPerson(requests: Flow<Base.PersonId>): Flow<Base.Person> = flow {
        requests.collect {
            emit(
                person {
                    name = "Rafael " + it.id
                    surname = "Iakupov"
                }
            )
        }
    }
}

fun main(args: Array<String>) {

    val personService = PersonService()
    val service = ServerBuilder
        .forPort(8081)
        .addService(personService)
        .build()

    service.start()
    while (true) {
    }
}